import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HonourAwardMedalComponent } from './honour-award-medal.component';

describe('HonourAwardMedalComponent', () => {
  let component: HonourAwardMedalComponent;
  let fixture: ComponentFixture<HonourAwardMedalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HonourAwardMedalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HonourAwardMedalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
