import { Component, OnInit } from '@angular/core';
import {RouterModule,Router} from '@angular/router';
import { GetService } from '../services/get.service';
import { PostService } from '../services/post.service';
import { ShareService } from '../services/share.service';
import { Commonbean } from '../commonbean';
import { AppComponent } from '../app.component';


@Component({
  selector: 'app-honour-award-medal',
  templateUrl: './honour-award-medal.component.html',
  styleUrls: ['./honour-award-medal.component.css']
})
export class HonourAwardMedalComponent implements OnInit {

  public commonbean = new Commonbean
  public user:any={};
  public  details:Commonbean[];
  
 
  public medals:any=[
    {
      medal_date:'',
      medal_desc:''
    }
  ];
  public awards:any=[];
    
  
  constructor(private router:Router,
    public getService:GetService,
    public postService:PostService,
    public shareService:ShareService,
    private app:AppComponent
  ) { }

  ngOnInit() {
    this.app.header='HONOURS & AWARDS';
    this.app.routerLink="services/details";
    this.user=this.shareService.getUser();
    this.getDetails();
    console.log('user ',this.user);
    this.medals=[];
    this.awards=[];
    
  }
  getDetails(){
    this.medals=[];
    this.awards=[];
    console.log('awards ',this.awards);
    console.log('medals ',this.medals);
    let count=0;
    let awd=0;
    this.getService.getDetails(4)
    .subscribe(data=>{
      this.details=data;
      console.log("details ",this.details);
       for(let i in this.details){
        console.log("award ",this.details[i].award_date ," ",this.details[i].award_desc);
        console.log("medal ",this.details[i].medal_date ," ",this.details[i].medal_desc);
  if(i=='0'){
      console.log("oooooo "+i);
       //award 
       let award={award_date:'', award_desc:'' };
       award.award_date=this.details[i].award_date;
       award.award_desc=this.details[i].award_desc;
       console.log("award ",award);
       
       this.awards.push(award);

        //medal 
        let medal={medal_date:'', medal_desc:'' };
        medal.medal_date=this.details[i].medal_date;
        medal.medal_desc=this.details[i].medal_desc;
        console.log("medal ",medal);
        this.medals.push(medal);

  }else{
    let isMedal=false;
    let isAward=false;
    console.log(i);
    for(let a in this.awards){
      if(this.awards[a].award_date == this.details[i].award_date && this.awards[a].award_desc == this.details[i].award_desc){
          isAward=true;
            break;
      }else{
        
      }
      console.log("award  for ",a);
    }



    if(!isAward){
         //award 
         let award={award_date:'', award_desc:'' };
         award.award_date=this.details[i].award_date;
         award.award_desc=this.details[i].award_desc;
         console.log("award ",award);
         
         this.awards.push(award);
  
    }


    for(let m in this.medals){
      console.log("medal  for ",m);
      if(this.medals[m].medal_date == this.details[i].medal_date && this.medals[m].medal_desc == this.details[i].medal_desc){
          isMedal=true;
          break;
      }else{
           //medal 
      }
  }
  if(!isMedal){
    let medal={medal_date:'', medal_desc:'' };
    medal.medal_date=this.details[i].medal_date;
    medal.medal_desc=this.details[i].medal_desc;
    console.log("medal ",medal);
    this.medals.push(medal);
  }
       
        

}}
       console.log('final  ',this.awards);
    console.log('medal ',this.medals);
    
    },error=>{
     console.log(error);
    });

    
  }


//    award_date:"2018-07-10"
// award_desc:"DGBR CC "

// medal_date:"2018-07-02"
// medal_desc:"SAINYA SEVA MEDAL"

}