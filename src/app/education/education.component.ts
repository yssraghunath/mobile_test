import { Component, OnInit } from '@angular/core';
import {RouterModule,Router} from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GetService } from '../services/get.service';
import { PostService } from '../services/post.service';
import { ShareService } from '../services/share.service';
import{Commonbean} from '../commonbean';
import { AppComponent } from '../app.component';
@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.css']
})
export class EducationComponent implements OnInit {
  public user:any={};
  public educationDetails:Commonbean[];
 
  constructor(private router:Router,
     public getService:GetService,
     public postService:PostService,
     public shareService:ShareService,
     private app:AppComponent
   ) {
     
   
  
  }
   
   
  
   ngOnInit() {
    this.app.header='EDUCATION DETAILS';
    this.app.routerLink="personal/details";
     this.user=this.shareService.getUser();
     console.log('user ',this.user);
     this.getEducationDetails();
   }
   
   getEducationDetails(){
     
    this.getService.getDetails(2)
    .subscribe(data=>{
      this.educationDetails=data;
       console.log('hhhhhhh',this.educationDetails) 
        },error=>{
     console.log('eeeee',error);
    });
   }
}
