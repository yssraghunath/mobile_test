import { Component, OnInit } from '@angular/core';
import {RouterModule,Router} from '@angular/router';
import { GetService } from '../services/get.service';
import { PostService } from '../services/post.service';
import { ShareService } from '../services/share.service';
import { Commonbean } from '../commonbean';
import { AppComponent } from '../app.component';


@Component({
  selector: 'app-grievance',
  templateUrl: './grievance.component.html',
  styleUrls: ['./grievance.component.css']
})
export class GrievanceComponent implements OnInit {

  public commonbean = new Commonbean
  public isShowDetais:boolean=false;
  //public user:any={};
  public user:any=[{
    grievance:''


}];


grievances :any={
    type:'',
    grienvanceMessage:'',
    gsno:''
}
public grienvanceType:any=[
  {
    id:'1',
    name:'Posting Related Complaints'
  },
  {
    id:'2',
    name:'Promotion Related Complaints'
  },{
    id:'3',
    name:'Service Record Related Complaints'
  }
]
  public  details:Commonbean[];
  constructor(private router:Router,
    public getService:GetService,
    public postService:PostService,
    public shareService:ShareService,
    private app:AppComponent
   
    
  ) { }

  ngOnInit() {
    this.app.header='GRIEVANCE';
    this.app.routerLink="user/details";
    this.user.grievance=this.grienvanceType[0].name;
    this.user=this.shareService.getUser();
    console.log('user ',this.user);
    
  }
  getDetails(){
    this.isShowDetais=true;
    this.getService.getDetails(22)
    .subscribe(data=>{
      this.details=data;
      //this.commonbean=this.details[0];
       console.log("aaaaaaaaaaaaaaaaaaaa",this.details);
    },error=>{
     console.log(error);
    });
   }

   saveGrivance(){
     debugger
     console.log(this.grievances);
     this.grievances.gsno=this.user.gsno;
     this.postService.saveGrievance(this.grievances)
    .map((response)=>response.json())
    .subscribe(data=>{
      console.log(data);
    },error=>{
      console.log(error);
    })
     
   }
 
}