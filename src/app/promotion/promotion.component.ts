import { Component, OnInit } from '@angular/core';
import {RouterModule,Router} from '@angular/router';
import { GetService } from '../services/get.service';
import { PostService } from '../services/post.service';
import { ShareService } from '../services/share.service';
import { Commonbean } from '../commonbean';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-promotion',
  templateUrl: './promotion.component.html',
  styleUrls: ['./promotion.component.css']
})
export class PromotionComponent implements OnInit {
  public commonbean = new Commonbean
  public commonbeans:Commonbean[];
  public user:any={};
  public  details:Commonbean[];
  constructor(private router:Router,
    public getService:GetService,
    public postService:PostService,
    public shareService:ShareService,
    private app:AppComponent
  ) { }

  ngOnInit() {
    this.app.header='PROMOTION ORDERS';
    this.app.routerLink="carrer/aspect";
    this.user=this.shareService.getUser();
    this.getDetails();
    console.log('user ',this.user);
    
  }
  getDetails(){
    this.getService.getDetails(7)
    .subscribe(data=>{
      this.details=data;
      this.commonbeans=this.details;
       console.log("aaaaaaaaaaaaaaaaaaaa",this.commonbean);
    },error=>{
     console.log(error);
    });
   }

}