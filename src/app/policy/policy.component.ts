import { Component, OnInit } from '@angular/core';
import {RouterModule,Router} from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GetService } from '../services/get.service';
import { PostService } from '../services/post.service';
import { ShareService } from '../services/share.service';
import { AppComponent } from '../app.component';
@Component({
  selector: 'app-policy',
  templateUrl: './policy.component.html',
  styleUrls: ['./policy.component.css']
})
export class PolicyComponent implements OnInit {

  public pdfSrc:string;
  public user:any={};
  public isShowMenu:boolean=true;

  public menuList:any=[
 
    {
      name:'BCA POLICY',
      url:'../assets/Policy/BCA_POLICY.pdf'
      },{
        name:'POSTING POLICY',
      url:'../assets/Policy/POSTING_POLICY.pdf'
    },{
      name:'CG LLP POLICY',
      url:'../assets/Policy/CG_LLP_POLICY.pdf'
  },{
    name:'DPC POLICY',
    url:'../assets/Policy/DPC_POLICY.pdf'
  },{
    name:'MACP POLICY',
    url:'../assets/Policy/MACP_POLICY.pdf'
  }
  ];
  constructor(private router:Router,
    public getService:GetService,
    public postService:PostService,
    public shareService:ShareService,
    private app:AppComponent
  ) { }


  ngOnInit() {
    this.app.header='POLICIES';
    this.app.routerLink="user/details";
    this.user=this.shareService.getUser();
    console.log('user ',this.user);
    
  }

  gotoPage(menu){
   /// this.router.navigateByUrl(menu.url);
   console.log(menu);
   this.isShowMenu=false;
   this.app.header=menu.name;
    this.pdfSrc=menu.url;
   }
   
   goBack(){
    this.isShowMenu=true;
    this.app.header='POLICIES';
   }

}
