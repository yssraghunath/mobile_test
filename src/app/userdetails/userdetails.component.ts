import { Component, OnInit } from '@angular/core';
import {RouterModule,Router} from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GetService } from '../services/get.service';
import { PostService } from '../services/post.service';
import { ShareService } from '../services/share.service';
import { AppComponent } from '../app.component';
@Component({
  selector: 'app-userdetails',
  templateUrl: './userdetails.component.html',
  styleUrls: ['./userdetails.component.css']
})
export class UserdetailsComponent implements OnInit {
 
  public user:any={};
  public changePassword:boolean=true;
  public upcommingEventList:any=[];
  public message:any;
  public menuList:any=[
    { name:'PROFILE',
      url:'/profile'
  },
  {
    name:'PERSONAL DETAILS',
    url:'/personal/details'
    
  },
    {
      name:'SERVICE DETAILS',
    url:'/services/details'
  },
    {
      name:'CAREER ASPECTS',
    url:'/carrer/aspect'
  },
    {
      name:'PAO CONRNER',
    url:'/pao/corner'
  },
    {
      name:'POLICIES',
    url:'/policy'
    },
    {
      name:'PTO STATUS',
    url:'/pto/status'
    },
    {
      name:'GRIEVANCE',
    url:'/grievance'
    },
    {
      name:'FORTHCOMMING COURSES',
    url:'upcomming/pdf'
    },
    {
      name:'COURSE RESULTS',
    url:'course/result'
    },
    {
    name:'BRO SONG',
    url:'/personal/details'
    },
  ]
  constructor(private router:Router,
    public getService:GetService,
    public postService:PostService,
    public shareService:ShareService,
    private app:AppComponent
  ) {
    this.changePassword=true;
   }

  ngOnInit() {
    this.app.routerLink="login";
    console.log(this.app.routerLink);
    this.changePassword=true;
    this.app.header='GREF अभिलेख';
    this.user=this.shareService.getUser();
    console.log('user ',this.user);
    this.getUpcommingEvent();
   
  }
  
  getUpcommingEvent(){
this.getService.getUpcommingEvent()
.subscribe(data =>{
this.upcommingEventList=data;
console.log(this.upcommingEventList);
  console.log(data);

},error =>{
  console.log(error);
})
  }

  // menu router link 
  gotoPage(menu){
    this.router.navigateByUrl(menu.url);
  }


}
