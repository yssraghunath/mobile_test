import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostingOrderComponent } from './posting-order.component';

describe('PostingOrderComponent', () => {
  let component: PostingOrderComponent;
  let fixture: ComponentFixture<PostingOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostingOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostingOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
