import { Component, OnInit } from '@angular/core';
import {RouterModule,Router} from '@angular/router';
import { GetService } from '../services/get.service';
import { PostService } from '../services/post.service';
import { ShareService } from '../services/share.service';
import { Commonbean } from '../commonbean';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-msl',
  templateUrl: './msl.component.html',
  styleUrls: ['./msl.component.css']
})
export class MslComponent implements OnInit {

  public commonbean = new Commonbean
  public user:any={};
  public  details:Commonbean[];
  constructor(private router:Router,
    public getService:GetService,
    public postService:PostService,
    public shareService:ShareService,
    private app:AppComponent
  ) { }

  ngOnInit() {
    this.app.header='MSL SENIORITY';
    this.app.routerLink="carrer/aspect";
    this.user=this.shareService.getUser();
    this.getDetails();
    console.log('user ',this.user);
    
  }
  getDetails(){
    this.getService.getDetails(12)
    .subscribe(data=>{
      this.details=data;
      this.commonbean=this.details[0];
       console.log("aaaaaaaaaaaaaaaaaaaa",this.commonbean);
    },error=>{
     console.log(error);
    });
   }

}