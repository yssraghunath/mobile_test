import { Component, OnInit } from '@angular/core';
import {RouterModule,Router} from '@angular/router';
import { GetService } from '../services/get.service';
import { PostService } from '../services/post.service';
import { ShareService } from '../services/share.service';
import { Commonbean } from '../commonbean';
import { AppComponent } from '../app.component';
@Component({
  selector: 'app-personal-info',
  templateUrl: './personal-info.component.html',
  styleUrls: ['./personal-info.component.css']
})
export class PersonalInfoComponent implements OnInit {
  public commonbean = new Commonbean
  public user:any={};
  public  details:Commonbean[];
  constructor(private router:Router,
    public getService:GetService,
    public postService:PostService,
    public shareService:ShareService,
    private app:AppComponent
   
  ) { }

  ngOnInit() {
    this.app.header='PERSONAL DETAILS';
    this.app.routerLink="personal/details";
    this.user=this.shareService.getUser();
    this.getDetails();
    
  } 
  
  getDetails(){
    
    this.getService.getDetails(1)
    .subscribe(data=>{
      this.details=data;
      this.commonbean=this.details[0];
       console.log("details",this.commonbean);
    },error=>{
     console.log(error);
    });
   }

}
