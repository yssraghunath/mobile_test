import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule,FormGroup,FormControl,ReactiveFormsModule} from '@angular/forms'
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import {RouterModule} from '@angular/router';
import { ErrordialogComponent } from './errordialog/errordialog.component';
import { UserdetailsComponent } from './userdetails/userdetails.component';
import { GetService } from './services/get.service';
import{PostService} from './services/post.service';
import{ShareService} from './services/share.service';
import{HttpModule} from '@angular/http';
import { ProfileComponent } from './profile/profile.component';
import { PersonalDetailsComponent } from './personal-details/personal-details.component';
import { ServicesdetailsComponent } from './servicesdetails/servicesdetails.component';
import { CarreraspectsComponent } from './carreraspects/carreraspects.component';
import { PaocornerComponent } from './paocorner/paocorner.component';
import { FamilydetailsComponent } from './familydetails/familydetails.component';
import { PersonalInfoComponent } from './personal-info/personal-info.component';
import { ParamountCardComponent } from './paramount-card/paramount-card.component';
import { PostingOrderComponent } from './posting-order/posting-order.component';
import { PobationComponent } from './pobation/pobation.component';
import { ProbationComponent } from './probation/probation.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { HonourAwardMedalComponent } from './honour-award-medal/honour-award-medal.component';
import { LeaveComponent } from './leave/leave.component';
import { PtoStatusComponent } from './pto-status/pto-status.component';
import { CoursesComponent } from './courses/courses.component';
import { PunishmentComponent } from './punishment/punishment.component';
import { EducationComponent } from './education/education.component';
import { PolicyComponent } from './policy/policy.component';
import { AcpMacpComponent } from './acp-macp/acp-macp.component';
import { PromotionComponent } from './promotion/promotion.component';
import { MslComponent } from './msl/msl.component';
import { PdfpolicyComponent } from './pdfpolicy/pdfpolicy.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { GpfWithdrawlStatusComponent } from './gpf-withdrawl-status/gpf-withdrawl-status.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { ChangePasswordComponent } from './change-password/change-password.component';
import {WebcamModule} from 'ngx-webcam';
import { CameraComponent } from './camera/camera.component';
import { GrievanceComponent } from './grievance/grievance.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { SliderComponent } from './slider/slider.component';
import { FourthpdfComponent } from './fourthpdf/fourthpdf.component';
import { CoursepdfComponent } from './coursepdf/coursepdf.component';
import { PayslippdfComponent } from './payslippdf/payslippdf.component';
import { Form16Component } from './form-16/./form-16.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    ErrordialogComponent,
    UserdetailsComponent,
    ProfileComponent,
    PersonalDetailsComponent,
    ServicesdetailsComponent,
    CarreraspectsComponent,
    PaocornerComponent,
    FamilydetailsComponent,
    PersonalInfoComponent,
    ParamountCardComponent,
    PostingOrderComponent,
    PobationComponent,
    ProbationComponent,
    ConfirmationComponent,
    HonourAwardMedalComponent,
    LeaveComponent,
    CoursesComponent,
    PtoStatusComponent,
    PunishmentComponent,
    EducationComponent,
    PolicyComponent,
    AcpMacpComponent,
    PromotionComponent,
    MslComponent,
    PdfpolicyComponent,
    GpfWithdrawlStatusComponent,
    ChangePasswordComponent,
    CameraComponent,
    GrievanceComponent,
    ForgetPasswordComponent,
    SliderComponent,
    FourthpdfComponent,
    CoursepdfComponent,
    Form16Component,
    PayslippdfComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AngularFontAwesomeModule,
    PdfViewerModule,
    WebcamModule,
    ModalModule.forRoot(),
    RouterModule.forRoot([
      {
        path:'',
        component:HomeComponent
      },
       {
        path:'register',
        component:RegisterComponent
      },
      {
        path:'login',
        component:LoginComponent
      },
      {
        path:'user/details',
        component:UserdetailsComponent
      },  {
        path:'profile',
        component:ProfileComponent
      },  {
        path:'personal/details',
        component:PersonalDetailsComponent
      },  {
        path:'services/details',
        component:ServicesdetailsComponent
      }, {
        path:'carrer/aspect',
        component:CarreraspectsComponent
      },{
        path:'pao/corner',
        component:PaocornerComponent
      },{
        path:"family/details",
        component:FamilydetailsComponent
      },{
        path:"personal/info",
        component:PersonalInfoComponent
      },{
        path:"paramount/card",
        component:ParamountCardComponent
      },{
        path:"posting/order",
        component:PostingOrderComponent
      },{
        path:"probation",
        component:ProbationComponent
      },{
        path:"confirmation",
        component:ConfirmationComponent
      },{
        path:"honors/award/medals",
        component:HonourAwardMedalComponent
      },{
        path:"leave/haa",
        component:LeaveComponent
      },{
        path:"paramount/card",
        component:ParamountCardComponent
      },{
        path:"courses",
        component:CoursesComponent
      },{
        path:"punishment",
        component:PunishmentComponent
      },{
        path:"education",
        component:EducationComponent
      },{
        path:'policy',
        component: PolicyComponent
      },{
        path:'acp/macp',
        component: AcpMacpComponent
      },{
        path:'promotion',
        component: PromotionComponent
      },{
        path:'msl',
        component: MslComponent
      },{
        path:'policy/pdf',
        component:PdfpolicyComponent
      },{
        path:'pto/status',
        component:PtoStatusComponent
      },{
        path:'gpf/withdrawl/status',
        component:GpfWithdrawlStatusComponent
      },{
        path:'change/password',
        component:ChangePasswordComponent
      },{
        path:'camera',
        component:CameraComponent
      },{
        path:'grievance',
        component:GrievanceComponent
      },
      {
        path:'forget/password',
        component:ForgetPasswordComponent
      },{
        path:'upcomming/pdf',
        component:FourthpdfComponent
      },{
        path:'course/result',
        component:CoursepdfComponent
      },{
        path:'pay-slip',
        component:PayslippdfComponent
      },{
        path:'form-16',
        component:Form16Component
      }
    ])
   
  ],
  entryComponents: [ErrordialogComponent],
  providers: [
    GetService,
    PostService,
    ShareService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
