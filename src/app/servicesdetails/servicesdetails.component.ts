import { Component, OnInit } from '@angular/core';
import {RouterModule,Router} from '@angular/router';
import { GetService } from '../services/get.service';
import { PostService } from '../services/post.service';
import { ShareService } from '../services/share.service';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-servicesdetails',
  templateUrl: './servicesdetails.component.html',
  styleUrls: ['./servicesdetails.component.css']
})
export class ServicesdetailsComponent implements OnInit {
  public user:any={};
  public menuList:any=[
 
    {
      name:'PARAMOUNT CARD',
      url:'paramount/card'
      
    },
      {
        name:'POSTING ORDER',
      url:'posting/order'
    }, {
      name:'PROBATION',
      url:'probation'
  },{
    name:'CONFIRMATION',
    url:'confirmation'
    
  },
    {
      name:'HONORS/AWARD/MEDALS',
    url:'honors/award/medals'
  }, {
    name:'LEAVE IN HAA',
    url:'leave/haa'
},{
  name:'COURSES',
url:'courses'
}, {
name:'PUNISHMENT',
url:'punishment'
}
];
constructor(private router:Router,
  public getService:GetService,
  public postService:PostService,
  public shareService:ShareService,
  private app:AppComponent
) { }


ngOnInit() {
   this.app.header='SERVICE DETAILS';
   this.app.routerLink="user/details";
  this.user=this.shareService.getUser();
  console.log('user ',this.user);
  
}
gotoPage(menu){
  this.router.navigateByUrl(menu.url);
}
}
