import { Component } from '@angular/core';
import {RouterModule,Router} from '@angular/router';
import { ShareService } from './services/share.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  public routerLink:any="/";
  public header:any="GREF अभिलेख";
  public changePassword:boolean=false;
  public user:any={};
  constructor(private router:Router,private shareService:ShareService){

  }

  logOut(){
    this.user=this.shareService.getUser();
    this.user={};
    this.shareService.setUser(this.user);
    this.router.navigateByUrl("login");

  }
  
  gotoBack(){
    console.log("rout "+this.routerLink)
    this.router.navigateByUrl(this.routerLink);
  }
}



