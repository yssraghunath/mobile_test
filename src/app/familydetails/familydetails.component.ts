import { Component, OnInit } from '@angular/core';
import {RouterModule,Router} from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GetService } from '../services/get.service';
import { PostService } from '../services/post.service';
import { ShareService } from '../services/share.service';
import { Commonbean } from '../commonbean';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-familydetails',
  templateUrl: './familydetails.component.html',
  styleUrls: ['./familydetails.component.css']
})
export class FamilydetailsComponent implements OnInit {

  public details:Commonbean[];
  public wife:any={};
  public children:any[];
  public dependents:any[];
  public user:any={};
 

  constructor(private router:Router,
    public getService:GetService,
    public postService:PostService,
    public shareService:ShareService,
    private app:AppComponent
   
  ) { }
  
  ngOnInit() {
    this.app.header='FAMILY DETAILS';
    this.app.routerLink="personal/details";
    this.user=this.shareService.getUser();
    console.log('user ',this.user);
    this.getFamilyDetails();
  }

  getFamilyDetails(){
    this.wife={};
    this.children=[];
    this.dependents=[];
    this.getService.getDetails(5)
   .subscribe(data=>{
     this.details=data;
     
    for(let detail in this.details){
      console.log(detail);
      if(this.details[detail].relation_name=='wife' || this.details[detail].relation_name=='Wife' ||this.details[detail].relation_name=='WIFE')
      {
        this.wife=this.details[detail]
      }else if(this.details[detail].relation_name=='daughter' || this.details[detail].relation_name=='Daughter' ||this.details[detail].relation_name=='DAUGHTER' ||this.details[detail].relation_name=='son' || this.details[detail].relation_name=='Son' ||this.details[detail].relation_name=='SON')
      {
        this.children.push(this.details[detail])
      } else{
        this.dependents.push(this.details[detail])
      }
    }
     
      console.log(this.details) 
       },error=>{
    console.log(error);
   });
  }


}
