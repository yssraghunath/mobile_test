import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { ShareService } from '../services/share.service';
import { HttpModule,JsonpModule, Jsonp, Http, Response, Headers, RequestOptions,Request } from '@angular/http';

import { Observable } from 'rxjs/Observable';
@Injectable()
export class GetService {


  public user:any={};
  private ip: any[];
  private myIP: any;
  private myPort: any;
  private myapp: any;
  public baseUrl = "";

 // baseUrl:any='http://103.192.67.20:8080/common.mobileapp.api-0.0.2/'
  pdfList:any=[
  {
    "id":"1",
    "url":"Policy/BCA_POLICY/BCA_POLICY.pdf"
  },{
    "id":"2",
    "url":""
  },{
    "id":"3",
    "url":""
  },{
    "id":"4",
    "url":""
  },{
    "id":"5",
    "url":""
  },
  ]




  constructor(private http: Http,private shareService:ShareService) { 
    this.getIp();


  }


  getIp() {

    return this.http.get("./assets/data.json").map((response) => response.json())
      .subscribe(data => {
        this.ip = data;
        console.log("ip ",this.ip);
        this.myIP = this.ip[0].myIP;
        this.myPort = this.ip[0].port;
        this.myapp = this.ip[0].app;
        if (this.myIP == "localhost") {
          this.baseUrl = 'http://' + this.myIP + ':' + this.myPort + '/' + this.myapp+'/querydata';
        } else {

          this.baseUrl = 'http://' + this.myIP + ':' + this.myPort + '/' + this.myapp+'/querydata';
        }
  });
  

  
}
getBaseUrl(){
  this.getIp();
  return this.baseUrl;
}


// getLoginDetails(){
//   console.log(this.baseUrl);
//     //return this.http.get(this.baseUrl + '/login/api?queryid=18&userid=punya&password=12345')
//     return this.http.get('http://103.192.67.20:8080/common.mobileapp.api-0.0.1/querydata/login/api?queryid=18&userid=punya&password=12345')
//       .map((response) => response.json());
 

// }
getUpcommingEvent(){
  return this.http.get(this.baseUrl+'/get/event2')
  .map((response)=>response.json());
}

getDetails(value){
  this.user=this.shareService.getUser();
  console.log(this.user.gsno);
  return this.http.get(this.baseUrl+'/api?id='+value+'&gsno='+this.user.gsno+'')
  .map((response)=>response.json());
}

getPdfDetails(value){

  let baseUrl1=this.baseUrl = 'http://' + this.myIP + ':' + this.myPort + '/' + this.myapp;
  return this.http.get(baseUrl1+'/'+this.pdfList[0].url)
  .map((response)=>response.json());
}

// getpdf(){
//   let url='http://103.192.67.20:8080/common.mobileapp.api-0.0.2/Policy/BCA_POLICY/BCA_POLICY.pdf';
//   let requestOptions = new RequestOptions({ headers:null, 
//     withCredentials: true });
//     return this.http.get(url, requestOptions)
//     .map(res => 
//     {
//      if(res != null)
//      { 
//       console.log("out",res);
//        return res.json();
//        //return true;
//      }
//      console.log("out",res);
//    })
 
//   } 




// getSmsList() {
//   this.loginId = this.itemDataServiceService.getUser();
//   return this.http.get(this.baseUrl + '/sms/smslist?loginId=' + this.loginId.loginId)
//     .map((response) => response.json());
// }

}



