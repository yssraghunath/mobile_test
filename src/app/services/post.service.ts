import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { HttpModule, Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { GetService } from './get.service';
import { HttpParams, HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
//import { Observable, throwError } from 'rxjs';
//import { catchError, retry } from 'rxjs/operators';



//http://117.205.10.74:8080/common.mobileapp.api-0.0.1/querydata/forgot/api?forgotQueryId=21&userid=punya&dob=1985-12-04&password=12345

@Injectable()
export class PostService {


  private headers=new Headers({'content-type':'application/json'});
  private options=new RequestOptions({headers:this.headers});
  public baseUrl:any;
  constructor(private getService:GetService,private http:Http) { }


  getUrl(){
      this.baseUrl=this.getService.getBaseUrl();
  }
  
  // getLoginDetails(){
  //   console.log(this.baseUrl);
  //     return this.http.post(this.baseUrl + '/login/api?queryid=18&userid=punya&password=12345')
  //       .map((response) => response.json());
   
  
  // }
  //login
doLogin(value:any) :Observable<any>{
  value.queryid=18;
  this.getUrl();
  console.log(this.baseUrl);
  console.log(value);

 return this.http.post(this.baseUrl + "/login/api?queryid=18&userid="+value.userId+"&imei=123&password="+value.password+"",Option);
  //return this.http.post('http://103.192.67.20:8080/common.mobileapp.api-0.0.1/querydata/login/api?queryid=18&userid=punya&imei=12345&password=12345',Option)
}

getPolicyPDF():Observable<any>{
  return this.http.post("http://103.192.67.20:8080/common.mobileapp.api-0.0.1/querydata/policy",Option)
   
  }


  //change password or forget password
  changePassword(value:any) :Observable<any>
{
 
  this.getUrl();
  return this.http.put(this.baseUrl +"/forgot/api?forgotQueryId=21&userid="+value.userid+"&dob="+value.dob+"&password="+value.newPassword+"",Option);
}
  
// forgetPassword(value:any) :Observable<any>
// {
// this.getUrl();
// return this.http.put(this.baseUrl +"/forgot/api?forgotQueryId=21&userid="+value.userid+"&dob="+value.dob+"&password="+value.newPassword+"",Option);
// }

saveGrievance(value){
  this.getUrl();

  return this.http.post(this.baseUrl+"/grievance?queryid=24&gsno="+value.gsno+"&grievance_type="+value.type+"&grievance_message="+value.grienvanceMessage+"",Option);
}

userRegistration(value:any):Observable<any>{
  console.log(value);
this.getUrl();
return this.http.post(this.baseUrl + "/signup/api?validateQueryId=19&signupQueryId=20&gsno=140648&userid=140648&dob=1958-04-21&mobile=8888888888&aadhar=78945612345&email=danishbbd%40gmail.com&imei=12345&password=140648",Option);
}

}



