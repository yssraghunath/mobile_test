import { Component, OnInit } from '@angular/core';
import {RouterModule,Router} from '@angular/router';
import { GetService } from '../services/get.service';
import { PostService } from '../services/post.service';
import { ShareService } from '../services/share.service';
import { Commonbean } from '../commonbean';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-payslippdf',
  templateUrl: './payslippdf.component.html',
  styleUrls: ['./payslippdf.component.css']
})
export class PayslippdfComponent implements OnInit {
  public commonbean = new Commonbean
  public commonbeans:Commonbean[];
  public user:any={};
  public  details:Commonbean[];
  public month:any;
  public year:any;
  public pdfSrc:any;
  payslip :any={
    month:'',
    year:'',
   
}
  constructor(private router:Router,
    public getService:GetService,
    public postService:PostService,
    public shareService:ShareService,
    private app:AppComponent
  
  ) { }

  ngOnInit() {
    this.user=this.shareService.getUser();
    this.app.routerLink="pao/corner";
    this.getDetails();
    console.log('user ',this.user);
    this.app.header='Pay-Slip';
  }
  getDetails(){
    this.getService.getDetails(6)
    .subscribe(data=>{
      this.details=data;
      this.commonbeans=this.details;
       console.log("aaaaaaaaaaaaaaaaaaaa",this.commonbeans);
    },error=>{
     console.log(error);
    });
   }


   public onChange1(event): void {  // event will give you full breif of action
    const newVal = event.target.value;
    console.log(newVal);
    this.month=newVal;

    if(this.year!='undefined' && this.month != 'undefined'){
    let filename='MPS_Y0000000_GS'+this.user.gsno+'H_'+this.month+'_'+this.year+'.pdf';  
    console.log(filename);

    this.pdfSrc="../assets/Salary/"+this.year+"/"+this.month+"/"+filename;
    }
    //\Salary\2015\09
    //MPS_Y0000000_GS111111H_09_2015
    // let filename='Form16_Y0000000_GS'+this.user.gsno+'H_'+newVal+'.pdf';
    // console.log(filename);
    // this.pdfSrc="../assets/Form16/"+filename

  }
  public onChange2(event): void {  // event will give you full breif of action
    const newVal = event.target.value;
    console.log(newVal);
    this.year=newVal;
    if(this.year!='undefined' && this.month != 'undefined'){

      let filename='MPS_Y0000000_GS'+this.user.gsno+'H_'+this.month+'_'+this.year+'.pdf';  
      console.log(filename);
  
      this.pdfSrc="../assets/Salary/"+this.year+"/"+this.month+"/"+filename;
     
    }
    // let filename='Form16_Y0000000_GS'+this.user.gsno+'H_'+newVal+'.pdf';
    // console.log(filename);
    // this.pdfSrc="../assets/Form16/"+filename

  }

  goBack(){
    this.router.navigateByUrl('user/details');
  }
}