import { Component, OnInit } from '@angular/core';
import {RouterModule,Router} from '@angular/router';
import { GetService } from '../services/get.service';
import { PostService } from '../services/post.service';
import { ShareService } from '../services/share.service';
import { Commonbean } from '../commonbean';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-paramount-card',
  templateUrl: './paramount-card.component.html',
  styleUrls: ['./paramount-card.component.css']
})
export class ParamountCardComponent implements OnInit {
  header:any="PARAMOUNT";
  
  public commonbean = new Commonbean
  public user:any={};
  public  details:Commonbean[];
  constructor(private router:Router,
    public getService:GetService,
    public postService:PostService,
    public shareService:ShareService,
    private app:AppComponent
   
  ) { }

  ngOnInit() {
    this.header="PARAMOUNT";
    this.app.routerLink="services/details";
    this.user=this.shareService.getUser();
    this.getDetails();
    console.log('user ',this.user);
    this.app.header=this.header;    
  }
  getDetails(){
    this.getService.getDetails(10)
    .subscribe(data=>{
      this.details=data;
    //  this.commonbean=this.details[0];
       console.log("aaaaaaaaaaaaaaaaaaaa",this.details);
    },error=>{
     console.log(error);
    });
   }

}
