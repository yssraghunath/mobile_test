import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParamountCardComponent } from './paramount-card.component';

describe('ParamountCardComponent', () => {
  let component: ParamountCardComponent;
  let fixture: ComponentFixture<ParamountCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParamountCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParamountCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
