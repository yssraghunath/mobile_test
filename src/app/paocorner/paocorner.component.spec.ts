import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaocornerComponent } from './paocorner.component';

describe('PaocornerComponent', () => {
  let component: PaocornerComponent;
  let fixture: ComponentFixture<PaocornerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaocornerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaocornerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
