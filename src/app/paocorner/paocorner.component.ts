import { Component, OnInit } from '@angular/core';
import {RouterModule,Router} from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GetService } from '../services/get.service';
import { PostService } from '../services/post.service';
import { ShareService } from '../services/share.service';
import { AppComponent } from '../app.component';
@Component({
  selector: 'app-paocorner',
  templateUrl: './paocorner.component.html',
  styleUrls: ['./paocorner.component.css']
})
export class PaocornerComponent implements OnInit {

  
  public user:any={};

  public menuList:any=[
 
  {
    name:'Pay Slip',
    url:'pay-slip'
    
  },
    {
      name:'FORM-16',
    url:'form-16'
  }, {
    name:'GPF WITHDRAWAL STATUS',
    url:'gpf/withdrawl/status'
}];
  constructor(private router:Router,
    public getService:GetService,
    public postService:PostService,
    public shareService:ShareService,
    private app:AppComponent
   
  ) { }


  ngOnInit() {
    this.app.header='PAO CORNER';
    this.app.routerLink="user/details";
    this.user=this.shareService.getUser();
    console.log('user ',this.user);
    
  }

  gotoPage(menu){
    this.router.navigateByUrl(menu.url);
  
   }

}
