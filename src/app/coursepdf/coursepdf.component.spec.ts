import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursepdfComponent } from './coursepdf.component';

describe('CoursepdfComponent', () => {
  let component: CoursepdfComponent;
  let fixture: ComponentFixture<CoursepdfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoursepdfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursepdfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
