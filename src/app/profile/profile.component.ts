import { Component, OnInit } from '@angular/core';
import {RouterModule,Router} from '@angular/router';
import { GetService } from '../services/get.service';
import { PostService } from '../services/post.service';
import { ShareService } from '../services/share.service';
import { AppComponent } from '../app.component';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  public user:any={};
 
  constructor(private router:Router,
    public getService:GetService,
    public postService:PostService,
    public shareService:ShareService,
    private app:AppComponent

  ) { }


  ngOnInit() {
    this.app.header='PROFILE DETAILS';
    this.app.routerLink="user/details";
    this.user=this.shareService.getUser();
    console.log('user ',this.user);
    this.getDetails();
    
  }

   // menu router link 
   gotoPage(menu){
  //s  this.router.navigateByUrl(menu.url);
  }
  getDetails(){
    this.getService.getDetails(1)
    .subscribe(data=>{
      console.log("data ",data);
    },error=>{
      console.log("error ",error);
    })
    
  }

    openCamera(){
      
      this.router.navigateByUrl('/camera');
    }
  
    update(){
      
    }
}
