import { Component, OnInit } from '@angular/core';
import {RouterModule,Router} from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GetService } from '../services/get.service';
import { PostService } from '../services/post.service';
import { ShareService } from '../services/share.service';
import { AppComponent } from '../app.component';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public routerLink:any="/";
public admin:any={
   userId:'',
  password:'',
  imei:''

};

public user:any={};
// public user:any={
//   gsno:'',
//   name:'',
//   userid:'',
// dob:'',
// };

  constructor(private router:Router,
    public getService:GetService,
    public postService:PostService,
    public shareService:ShareService,
    private app:AppComponent
  ) { }

  ngOnInit() {
    this.app.header='GREF अभिलेख';
    console.log(this.routerLink);
  }
  login(){
    
   console.log("admin ",this.admin);
   this.postService.doLogin(this.admin).map((response) => response.json()).
   subscribe(data=>{
     
    console.log("ddatddd ",data.status);
    console.log("ddatddd ",data[0].gsno);
    if(data[0].gsno !==null && data[0].gsno !==undefined){
      // this.user.gsno=data[0].gsno;
      // this.user.name=data[0].name;
      // this.user.userid=data[0].userid;
      // this.user.dob=data[0].dob;
      this.user=data[0];
      
      this.shareService.setUser(this.user);
      this.router.navigate(['/user/details']);
    }

   
  },error=>{
    console.log("ddat eee",error.status);
  })
  

   
   
  }
}
