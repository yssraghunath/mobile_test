import { Component, OnInit } from '@angular/core';
import {RouterModule,Router} from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GetService } from '../services/get.service';
import { PostService } from '../services/post.service';
import { ShareService } from '../services/share.service';

@Component({
  selector: 'app-pdfpolicy',
  templateUrl: './pdfpolicy.component.html',
  styleUrls: ['./pdfpolicy.component.css']
})
export class PdfpolicyComponent implements OnInit {
//pdfSrc: string = 'http://103.192.67.20:8080/common.mobileapp.api-0.0.2/Policy/BCA_POLICY/BCA_POLICY.pdf';
pdfSrc:string
fileDetails:any={};
baseUrl:any='http://103.192.67.20:8080/common.mobileapp.api-0.0.2/'
pdfList:any=[
{
  "id":"1",
  "url":"../assets/Policy/BCA_POLICY.pdf"
},{
  "id":"2",
  "url":"../assets/Policy/CG_LLP_POLICY.pdf"
},{
  "id":"3",
  "url":"../assets/Policy/DPC_POLICY.pdf"
},{
  "id":"4",
  "url":"../assets/Policy/MACP_POLICY.pdf"
},{
  "id":"5",
  "url":"../assets/Policy/POSTING_POLICY.pdf"
}
]

public user:any={};
 
 
constructor(private router:Router,
   public getService:GetService,
   public postService:PostService,
   public shareService:ShareService,
  
 ) { }

 ngOnInit() {
  this.user=this.shareService.getUser();
  console.log('user ',this.user);
  this.setPdfUrl(0);
 //  this.getPolicyPDF();
}

 setPdfUrl(value){

  //   console.log("url ",this.baseUrl+this.pdfList[value].url);
  //  // this.pdfSrc=this.baseUrl+this.pdfList[value].url;
  //  this.getService.getpdf()
  //  .subscribe(data =>{
  //    console.log("dafddd"+data);
  //  },error=>{
  //    console.log("eror "+error);
  //  })
  //F:\@ ntpc gui\mobile_app\mobile_test\src\assets\course\COURSE_RESULT.pdf
  //this.pdfSrc="../assets/course/COURSE_RESULT.pdf";
  this.pdfSrc=this.pdfList[0].url;
  console.log("url "+this.pdfSrc);
  }

  // getPolicyPDF(){
  //   this.postService.getPolicyPDF()
  //   .map((response) => response.json())
  //   .subscribe(data =>{
  //     console.log("dtata ",data)
  //    console.log("file details "+data.document)
  //   },error=>{
  //     console.log("error "+error);
  //   })
  // }
}
