import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcpMacpComponent } from './acp-macp.component';

describe('AcpMacpComponent', () => {
  let component: AcpMacpComponent;
  let fixture: ComponentFixture<AcpMacpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcpMacpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcpMacpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
