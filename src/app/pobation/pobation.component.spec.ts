import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PobationComponent } from './pobation.component';

describe('PobationComponent', () => {
  let component: PobationComponent;
  let fixture: ComponentFixture<PobationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PobationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PobationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
