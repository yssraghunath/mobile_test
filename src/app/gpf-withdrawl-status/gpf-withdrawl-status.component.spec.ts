import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpfWithdrawlStatusComponent } from './gpf-withdrawl-status.component';

describe('GpfWithdrawlStatusComponent', () => {
  let component: GpfWithdrawlStatusComponent;
  let fixture: ComponentFixture<GpfWithdrawlStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpfWithdrawlStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpfWithdrawlStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
