import { Component, OnInit } from '@angular/core';
import {RouterModule,Router} from '@angular/router';
import { GetService } from '../services/get.service';
import { PostService } from '../services/post.service';
import { ShareService } from '../services/share.service';
import { Commonbean } from '../commonbean';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-fourthpdf',
  templateUrl: './fourthpdf.component.html',
  styleUrls: ['./fourthpdf.component.css']
})
export class FourthpdfComponent implements OnInit {
 public pdfSrc:any;
 constructor(private router:Router,
  public getService:GetService,
  public postService:PostService,
  public shareService:ShareService,
  private app:AppComponent
 
  
) { }

  ngOnInit() {
    this.pdfSrc="../assets/Upcoming/UPCOMING_COURSE.pdf";
    this.app.routerLink="user/details";
  }

  goBack(){
    this.router.navigateByUrl('user/details');
  }
}
