import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FourthpdfComponent } from './fourthpdf.component';

describe('FourthpdfComponent', () => {
  let component: FourthpdfComponent;
  let fixture: ComponentFixture<FourthpdfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FourthpdfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FourthpdfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
