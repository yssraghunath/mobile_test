import { Component, OnInit } from '@angular/core';
import {RouterModule,Router} from '@angular/router';
import { GetService } from '../services/get.service';
import { PostService } from '../services/post.service';
import { ShareService } from '../services/share.service';
import { Commonbean } from '../commonbean';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
 

  public user1 :any = {};
  public commonbean = new Commonbean
  public user:any={};
  public userId:any='';
  public dob:any='';
  public newPassword:any='';
  public confirmPassword:any='';
  public checkResult:boolean=false;
  public checkSuccesResult:boolean=false;
  public checkErrorResult:boolean=false;
  public  details:Commonbean[];
  constructor(private router:Router,
    public getService:GetService,
    public postService:PostService,
    public shareService:ShareService,
    private app:AppComponent
   
  ) { }

  ngOnInit() {
    this.app.header='CHANGE PASSWORD';
    this.user=this.shareService.getUser();
  
    console.log('user ',this.user);

    
  }

   changePassword(){ 
    
    if(this.user.newPassword  == this.user.confirmPassword){
     
    this.postService.changePassword(this.user).map((response) => response.json()).
    subscribe(data=>{
 console.log("aaaaa",data)
 if(data.status==200){
  //   alert(data.status);
    this.checkSuccesResult=true;
    setTimeout(() =>{
      this.checkSuccesResult=true;
      },5000);
   }
  
  },error=>{
      console.log(error);
  } 
);
}
else{
  this.checkErrorResult=true;
setTimeout(() =>{
this.checkErrorResult=false;
},5000);
}

   }
  }
