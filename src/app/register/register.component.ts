import { Component, OnInit } from '@angular/core';

import { ErrordialogComponent } from '../errordialog/errordialog.component';
import {RouterModule,Router} from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GetService } from '../services/get.service';
import { PostService } from '../services/post.service';
import { ShareService } from '../services/share.service';
import { AppComponent } from '../app.component';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
 public admin:any=[];
  constructor(private router:Router,
    public getService:GetService,
    public postService:PostService,
    public shareService:ShareService,
    private app:AppComponent
  ) {}
  ngOnInit() {
    this.app.routerLink="/";
  }



  register(){
   this.postService.userRegistration(this.admin)
   .map((response) => response.json())
   .subscribe(data=>{
         console.log(data);
    this.router.navigateByUrl('login')
  },error=>{
    console.log(error);
  }
  )
  }
  showError(error : string) : void {
  
    console.log("heee");
    // this.dialog.open(ErrordialogComponent, {
    //   data: {errorMsg: error} ,width : '250px'
    // });
  }
}
