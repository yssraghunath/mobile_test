import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PtoStatusComponent } from './pto-status.component';

describe('PtoStatusComponent', () => {
  let component: PtoStatusComponent;
  let fixture: ComponentFixture<PtoStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PtoStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PtoStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
