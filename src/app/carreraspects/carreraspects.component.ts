import { Component, OnInit } from '@angular/core';
import {RouterModule,Router} from '@angular/router';
import { GetService } from '../services/get.service';
import { PostService } from '../services/post.service';
import { ShareService } from '../services/share.service';
import { AppComponent } from '../app.component';
@Component({
  selector: 'app-carreraspects',
  templateUrl: './carreraspects.component.html',
  styleUrls: ['./carreraspects.component.css']
})
export class CarreraspectsComponent implements OnInit {

  public user:any={};

  public menuList:any=[
 
  {
    name:'ACP/MACP',
    url:'acp/macp'
    
  },
    {
      name:'PROMOTION',
    url:'promotion'
  }, {
    name:'MSL',
    url:'msl'
}];
  constructor(private router:Router,
    public getService:GetService,
    public postService:PostService,
    public shareService:ShareService,
    private app:AppComponent
   
  ) { }


  ngOnInit() {
    this.app.header='CAREER ASPECTS';
    this.app.routerLink="user/details";
    this.user=this.shareService.getUser();
    console.log('user ',this.user);
    
  }

  gotoPage(menu){
    this.router.navigateByUrl(menu.url);
  
   }

}
