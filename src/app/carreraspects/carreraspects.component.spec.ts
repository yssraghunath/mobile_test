import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarreraspectsComponent } from './carreraspects.component';

describe('CarreraspectsComponent', () => {
  let component: CarreraspectsComponent;
  let fixture: ComponentFixture<CarreraspectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarreraspectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarreraspectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
