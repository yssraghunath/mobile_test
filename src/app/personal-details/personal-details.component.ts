import { Component, OnInit } from '@angular/core';
import {RouterModule,Router} from '@angular/router';
import { GetService } from '../services/get.service';
import { PostService } from '../services/post.service';
import { ShareService } from '../services/share.service';
import { AppComponent } from '../app.component';
@Component({
  selector: 'app-personal-details',
  templateUrl: './personal-details.component.html',
  styleUrls: ['./personal-details.component.css']
})
export class PersonalDetailsComponent implements OnInit {

  public user:any={};

  public menuList:any=[
 
  {
    name:'PERSONAL DETAILS',
    url:'personal/info'
    
  },
    {
      name:'FAMILY DETAILS',
    url:'family/details'
  }, {
    name:'EDUCATION',
    url:'education'
}];
  constructor(private router:Router,
    public getService:GetService,
    public postService:PostService,
    public shareService:ShareService,
    private app:AppComponent
   
  ) { }


  ngOnInit() {
    this.app.header='PERSONAL DETAILS';
    this.app.routerLink="user/details";
    this.user=this.shareService.getUser();
    console.log('user ',this.user);
    
  }

   // menu router link 
   gotoPage(menu){
   this.router.navigateByUrl(menu.url);
  // this.getService.getDetails(menu.url)
  // .subscribe(data => {
  //   console.log(data);
  // },error => {
  //   console.log(error);
  // });

  }
}