import { Component, OnInit, TemplateRef } from '@angular/core';
import {RouterModule,Router} from '@angular/router';
import { GetService } from '../services/get.service';
import { PostService } from '../services/post.service';
import { ShareService } from '../services/share.service';
import { Commonbean } from '../commonbean';
import { AppComponent } from '../app.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';


@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {

  modalRef: BsModalRef;
  public commonbean = new Commonbean
  public user:any={};
  public admin:any={
    userid:'',
    newPassword:'',
    dob:''
 
 };
 public checkResult:boolean=false;
 public checkSuccesResult:boolean=false;
 public checkErrorResult:boolean=false;
  public  details:Commonbean[];
  constructor(private router:Router,
    public getService:GetService,
    public postService:PostService,
    public shareService:ShareService,
    private app:AppComponent,private modalService: BsModalService
   
    
  ) { }

  ngOnInit() {
    this.app.header='FORGET PASSWORD';
    this.user=this.shareService.getUser();
    this.getDetails();
    console.log('user ',this.user);
    
  }
  getDetails(){
    this.getService.getDetails(22)
    .subscribe(data=>{
      this.details=data;
      //this.commonbean=this.details[0];
       console.log("aaaaaaaaaaaaaaaaaaaa",this.details);
    },error=>{
     console.log(error);
    });
   }

   forgetPassword(){ 
    
   
    this.postService.changePassword(this.admin).map((response) => response.json()).
    subscribe(data=>{
 console.log("aaaaa",data)
 if(data.status==200){
  //   alert(data.status);
    this.checkSuccesResult=true;
    setTimeout(() =>{
      this.checkSuccesResult=true;
      },5000);
   }
  
  },error=>{
      console.log(error);
  } 
);
   }

   openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
  }