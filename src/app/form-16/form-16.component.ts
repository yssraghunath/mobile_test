import { Component, OnInit } from '@angular/core';
import {RouterModule,Router} from '@angular/router';
import { GetService } from '../services/get.service';
import { PostService } from '../services/post.service';
import { ShareService } from '../services/share.service';
import { Commonbean } from '../commonbean';
import { AppComponent } from '../app.component';


@Component({
  selector: 'app-form-16',
  templateUrl: './form-16.component.html',
  styleUrls: ['./form-16.component.css']
})
export class Form16Component implements OnInit {
  public commonbean = new Commonbean
  public commonbeans:Commonbean[];
  public user:any={};
  public  details:Commonbean[];
  public pdfSrc:any;
  form16 :any={

    year:'',
   
}
  constructor(private router:Router,
    public getService:GetService,
    public postService:PostService,
    public shareService:ShareService,
    private app:AppComponent
  
  ) { }

  ngOnInit() {
    this.app.routerLink="pao/corner";
    this.user=this.shareService.getUser();
    this.getDetails();
    console.log('user ',this.user);
    this.app.header='Form-16';
  }

  //Form16_Y0000000_GS140756H_2015_2016
  getDetails(){
    this.getService.getDetails(6)
    .subscribe(data=>{
      this.details=data;
      this.commonbeans=this.details;
       console.log("aaaaaaaaaaaaaaaaaaaa",this.commonbeans);
    },error=>{
     console.log(error);
    });
   }

   public onChange(event): void {  // event will give you full breif of action
    const newVal = event.target.value;
    console.log(newVal);
    let filename='Form16_Y0000000_GS'+this.user.gsno+'H_'+newVal+'.pdf';
    console.log(filename);
    this.pdfSrc="../assets/Form16/"+filename

  }

  goBack(){
    this.router.navigateByUrl('user/details');
  }
}